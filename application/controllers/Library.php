<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Library
extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->table = "wstb_library";
	}
	public function getlist()
	{
		$url = base_url() . 'public/library/';
		$sql = "SELECT id, page_id, 
		(CASE WHEN images!='' THEN CONCAT('" . $url . "', images) ELSE '' END) AS images, 
		name, value, description, orders, maker_date, type, status 
		FROM " . $this->table . " 
		ORDER BY maker_date DESC";

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$list = ($list != null) ? $list :  array();

		$message = $this->lang->line('success');

		$this->responsesuccess($message, $list);
	}
	public function getrow()
	{

		$id = $this->params['id'];

		$sql = "SELECT * FROM " . $this->table . " WHERE id=" . $id;

		$query = $this->db->query($sql);

		$list = $query->row_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}
	public function remove()
	{

		$id = isset($this->params['id']) && $this->params['id'] > 0 ? $this->params['id'] : 0;

		$is = false;

		if ($id > 0) {

			$sql = "SELECT * FROM " . $this->table . " WHERE id=" . $id;

			$list = $this->db->query($sql)->row_object();

			$img = $list->value;

			$pathold = 'public/library/' . $img;

			$this->db->where('id', $id);

			$is = $this->db->delete($this->table);
		}

		if ($is == true) {

			@unlink($pathold);

			$message = $this->lang->line('success');

			$this->responsesuccess($message);
		} else {

			$message = $this->lang->line('failure');

			$this->responsefailure($message);
		}
	}
	public function process()
	{

		$data = $this->getdata();

		$id = isset($this->params['id']) && $this->params['id'] > 0 ? $this->params['id'] : 0;

		$is = false;

		$message = $this->lang->line('failure');

		if ($data !== null) {
			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);
			if(array_key_exists('images', $data)) {
				$data['images'] = $this->processimages($data['images']);
			}
			$data['maker_id'] = $this->session->userdata('user_id');
			$data['maker_date'] = date('Y-m-d H:i:s');
			$sql = "SELECT COUNT(id) AS count FROM " . $this->table . " WHERE name='" . $data['name'] . "'";
			if ($id > 0) {
				$sql .= " AND id!=" . $id;
			}
			if ($this->db->query($sql)->row_object()->count == 0) {
				if ($id == 0) {
					$is = $this->db->insert($this->table, $data);
				} else {
					$this->db->where('id', $id);
					$is = $this->db->update($this->table, $data);
				}
				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
			}
		}
		$is == true ? $this->responsesuccess($message) : $this->responsefailure($message);	
	}
}
