<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Product extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->table = "pdtb_product";
	}
	public function getlist()
	{

		$sql = "SELECT t1.id, t1.name, t1.code, t1.images, 
		
		t1.page_id, t1.status, t1.maker_date, t2.name AS namegroup		
		
		FROM " . $this->table . " AS t1 
		
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id AND t2.id !=0";

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$this->responsesuccess(null, $list);
	}

	public function getrow()
	{
		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$sql = "SELECT * FROM " . $this->table . " where id=" . $id;

		$query = $this->db->query($sql);

		$list = $query->row_object();

		$this->responsesuccess(null, $list);
	}

	public function process()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$is = false;

		$message = $this->lang->line('failure');

		if ($data !== null) {

			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);

			$data['maker_id'] = $this->session->userdata('user_id');

			$data['maker_date'] = date('Y-m-d H:i:s');

			$data['code'] = isset($data['code']) && $data['code'] != '' ? $data['code'] : time();

			if (array_key_exists('price_sale', $data) && $data['price_sale'] > 0 && $data['price_sale'] != null) {

				$data['percent'] = 100 - round(((($data['price_sale'] * 100) / $data['price'])));
			} else {

				$data['percent'] = 0;
			}

			$data['link'] = (array_key_exists('link', $data) && strlen($data['link']) > 1) ? removesign($data['link']) : removesign($data['name']);

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE link='" . $data['link'] . "'";

			if ($id > 0) {

				$sql .= " and id!=" . $id;
			}

			if ($this->db->query($sql)->row_object()->count == 0) {

				$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE code ='" . $data['code'] . "'";

				if ($id > 0) {

					$sql .= " and id!=" . $id;
				}
				if ($this->db->query($sql)->row_object()->count == 0) {

					if ($id == 0) {

						$is = $this->db->insert($this->table, $data);

						$id = $this->db->insert_id();
					} else {

						$this->db->where('id', $id);

						$is = $this->db->update($this->table, $data);
					}

					$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
				} else {

					$message = 'Mã SKU đã tồn tại';
				}
			} else {

				$message = $this->lang->line('checkExitname');
			}
		}

		$is == true ? $this->responsesuccess($message, $id) : $this->responsefailure($message);
	}


	public function  updateImages()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$is = false;

		if ($data !== null) {

			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);

			if (array_key_exists('images', $data)) {

				$data['images'] = $this->processimages($data['images']);
			} else {

				$data['images'] = '';
			}


			if (array_key_exists('listimages', $data)) {

				$data['listimages'] = $this->processimages($data['listimages']);
			} else {

				$data['listimages'] = '';
			}

			if ($id > 0) {

				$this->db->where('id', $id);

				$is = $this->db->update($this->table, $data);
			}
		}

		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is) {

			$this->responsesuccess($message, $id);
		} else {

			$this->responsefailure($message);
		}
	}

	public function updateSeo()
	{

		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$data = $this->getdata();

		$is = false;

		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('description' => $data['description'], 'keywords' => $data['keywords']));

		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		$is == true ? $this->responsesuccess($this->lang->line('success')) : $this->responsefailure($this->lang->line('failure'));
	}
	public function remove()
	{

		$data = $this->getdata();

		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$is = false;

		$message = $this->lang->line('failure');

		if ($id > 0) {

			$sql = "select * from " . $this->table . " where id=" . $id;

			$list = $this->db->query($sql)->row_object();

			$this->db->where('id', $id);

			$is = $this->db->delete($this->table);

			if (isset($list->images) && strlen($list->images) > 4) {

				@unlink('public/products/' . $list->images);
			}
			if (isset($list->banner) && strlen($list->banner) > 4) {

				@unlink('public/products/' . $list->banner);
			}
			if (isset($list->images_specification) && strlen($list->images_specification) > 4) {

				@unlink('public/specification/' . $list->images_specification);
			}
			if (isset($list->listimages) && strlen($list->listimages) > 4) {

				$listimages	= json_decode($list->listimages);

				if (count($listimages) > 0) {

					foreach ($listimages as $k) {

						@unlink('public/products/' . $k);
					}
				}
			}
		}

		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is == true) {

			$this->responsesuccess($message);
		} else {
			$this->responsefailure($message);
		}
	}

	public function changeStatus()
	{

		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$status = isset($this->params['status']) ? $this->params['status'] : 0;

		$is = false;

		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('status' => $status));

		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is == true) {

			$this->responsesuccess($message);
		} else {
			$this->responsefailure($message);
		}
	}

	public function updatePrice()
	{

		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$data = $this->getdata();

		$is = false;

		if (is_array($data) && array_key_exists('price_sale', $data) && $data['price_sale'] > 0 && $data['price_sale'] != null) {

			$data['percent'] = 100 - round(((($data['price_sale'] * 100) / $data['price'])));
		} else {
			$data['percent'] = 0;
		}

		$this->db->where('id', $id);

		$is = $this->db->update($this->table, $data);

		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		$is == true ? $this->responsesuccess($this->lang->line('success')) : $this->responsefailure($this->lang->line('failure'));
	}

	public function updateAttribute()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$is = false;
		
		if (count($data) > 0 && $id > 0) {
			
			for($i = 0; $i < count($data); $i++){
				$data[$i]['maker_id'] = $this->session->userdata('user_id');
				$data[$i]['maker_date'] = date('Y-m-d H:i:s');
				unset($data[$i]['name']);
				unset($data[$i]['list']);
				
				$where = " product_id =" .$data[$i]['product_id'] . "  AND attribute_id =" . $data[$i]['attribute_id'] . "";
				
				$sql = "select count(id) as count from pdtb_product_att where ". $where;
				
				if ($this->db->query($sql)->row_object()->count == 0) {
						
					$this->db->insert('pdtb_product_att', $data[$i]);
				
				}else{
					
					$this->db->where($where);
					
					$this->db->update('pdtb_product_att', $data[$i]);
				}	
				
			}
			
			$is = true;	
		}
		$is == true ? $this->responsesuccess($this->lang->line('success')): $this->responsefailure($this->lang->line('failure'));
	}
}
