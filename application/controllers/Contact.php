<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Contact extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->table = "wstb_contact";
	}
	public function getlist()
	{

		$sql = "select t1.*, t2.name as user_name, t3.name as user_name_send from " . $this->table . " as t1 
		left join hrtb_user as t2 on t1.maker_id = t2.id
		left join hrtb_user as t3 on t1.maker_id = t3.id";

		$list = $this->db->query($sql)->result_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}
	public function getrow()
	{

		$id = $this->params['id'];

		$sql = "select * from " . $this->table . " where id=" . $id;

		$list = $this->db->query($sql)->row_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}

	public function process()
	{
		$data = $this->getdata();

		$id = $this->params['id'];

		$is = false;

		if ($data != null && $id > 0) {

			$data['maker_id'] = $this->session->userdata('user_id');

			$data['day_check'] = date('Y-m-d H:i:s');

			$data['checked'] = 1;

			$this->db->where('id', $id);

			$is = $this->db->update($this->table, $data);
		}

		if ($is == true) {

			$this->responsesuccess($this->lang->line('success'));
		} else {

			$this->responsefailure($this->lang->line('failure'));
		}
	}
	public function remove()
	{
		$id = $this->params['id'];

		$is = false;

		if ($id > 0) {

			$this->db->where('id', $id);

			$is = $this->db->delete($this->table);

			$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
		}
		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}


	public function sendmailContact()
	{
		$data = $this->getdata();
		$is = false;

		if ($data != null) {

			$data['day_send'] = date('Y-m-d H:i:s');

			$data['maker_id'] = $this->session->userdata('user_id');

			$id = $data['id'];

			$this->db->where('id', $id);

			$this->db->update($this->table, $data);

			$emailto = $data['email'];

			$subject = $data['subject_send'];

			$message = $data['message_send'];

			$is = $this->sendmail($emailto, $subject, $message);
		}
        $message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
}
