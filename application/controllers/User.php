<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->table = "hrtb_user";

		$this->history = "./public/history/avatar/";

		$this->avatar = "./public/avatar/";

		if (!is_dir($this->history)) {

			mkdir($this->history, 0757);
		}
		if (!is_dir($this->avatar)) {

			mkdir($this->avatar, 0757);
		}
	}

	public function getlist()
	{
		$sql = "SELECT id ,avatar  , name , phone , email ,status, pin, maker_date FROM " . $this->table . " ORDER BY maker_date DESC";

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}

	public function getrow()
	{
		$id = $this->params['id'];

		$sql = "SELECT *  FROM " . $this->table . " WHERE id=" . $id;

		$query = $this->db->query($sql);

		$list = $query->row_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}
	public function process()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) > 0 ? $this->params['id'] : 0;

		$is = false;

		if ($data !== null) {

			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);

			if (array_key_exists('avatar', $data)) {

				$data['avatar'] = $this->processimages($data['avatar']);
			} else {

				$data['avatar'] = '';
			}

			$data['code'] = time();

			$data['maker_id'] = $this->session->userdata('user_id');

			$data['maker_date'] = date('Y-m-d H:i:s');

			$email = array_key_exists('email', $data) ?  $data['email'] : '';

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE email='" . $email . "'";

			if ($id > 0) {

				$sql .= " AND id!=" . $id;
			}

			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {

					$data['password'] =  array_key_exists('password', $data) ?  sha1($data['password']) : sha1('admin@123');

					$is = $this->db->insert($this->table, $data);

				} else {

					if (array_key_exists('password', $data)) {

						unset($data['password']);
					}

					$this->db->where('id', $id);

					$is = $this->db->update($this->table, $data);

				}

				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
				
			} else {

				$message = $this->lang->line('checkExitEmail');
			}

		} else {

			$message = $this->lang->line('failure');

		}
		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
	public function changepassword()
	{
		$is = false;

		$data = $this->getdata();

		$id = $this->params['id'] && $this->params['id'] > 0 ? $this->params['id'] : 0;

		if ($data !== null) {

			$this->db->where('id', $id);

			$is = $this->db->update($this->table, array('password' => sha1($data['password'])));

		}

		$message = $is == true ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
	
	public function changeToken(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$pin = isset($this->params['pin']) ? $this->params['pin'] : 0;
		
		$is = false;
		
		if( $pin == 1){
            $sql = "UPDATE ".$this->table." SET pin = 0 ";
            $is = $this->db->query($sql);
        }
		
		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('pin' => $pin));
		
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is == true) {

			$this->responsesuccess($message);
			
		} else {
			$this->responsefailure($message);
		}
	}
	
	public function remove()
	{
		$is = false;

		$id = $this->params['id'] && $this->params['id'] > 0 ? $this->params['id'] : 0;

		if ($id > 0) {

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE  is_delete = 1 AND id=" . $id;

			if ($this->db->query($sql)->row_object()->count == 0) {

				$this->db->where('id', $id);

				$is = $this->db->delete($this->table);

				$message = $is == true ? $this->lang->line('success') : $this->lang->line('failure');

			} else {

				$message = $this->lang->line('isDelete');
			}

		} else {
			
			$message = $this->lang->line('failure');
		}

		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
}
