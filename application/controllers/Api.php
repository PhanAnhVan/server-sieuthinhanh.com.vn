<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
	}
	public function logoutadmin()
	{

		@$this->session->sess_destroy();

		$this->responsesuccess(null, array());
	}
	public function checklogin()
	{

		$data = $this->getdata();
		$list = '';
		$skip = false;

		if ($data !== null) {

			$password = isset($data['password']) ? $data['password'] : '';
			$email = isset($data['email']) ? $data['email'] : '';
			$sql = "SELECT count(id) as count  FROM hrtb_user WHERE  status = 1 AND email='" . $email . "'  AND password ='" . $password . "'";

			$query = $this->db->query($sql);
			$skip = $this->db->query($sql)->row_object()->count == 1 ? true : false;

			if ($skip == true) {

				$sql = "SELECT * FROM hrtb_user WHERE email='" . $data['email'] . "' AND status=1 AND password ='" . $password . "'";

				$query = $this->db->query($sql);
				$list = $query->row_object();
				$this->session->set_userdata('user', true);
				$this->session->set_userdata('user_id', $list->id);
				$this->session->set_userdata('user_name', $list->name);
			}
		}

		$list = array('skip' => $skip, 'data' => $list);
		echo json_encode($list);
	}
	//NOT DELETE - NOT UPDATE - TIENNV

	// GET MENU - 
	public function getmenu()
	{

		$position = $this->params['position'];

		$url = base_url() . 'public/pages/';

		$listid = array();

		$sql = "SELECT container FROM wstm_page_group WHERE position='" . $position . "' AND status=1";

		$query = $this->db->query($sql);

		$list = $query->row_object();

		if ($list) {

			$listid = (strlen($list->container) > 1) ? json_decode($list->container) : array();
		}

		$result = array();

		if (count($listid) > 0) {

			$sql = "SELECT t1.id ,t1.name, t1.description,t1.title, t1.type, t1.parent_id,t1.link, t1.code,
			
			(CASE WHEN t1.orders = 0 THEN t1.id ElSE t1.orders END) AS orderby,
			
			(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) as images,

			t2.link AS parent_link
			
			FROM wstm_page  AS t1
			
			LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id
			
			WHERE t1.id IN (" . implode(',', $listid) . ") AND t1.status=1 ORDER BY orderby ASC";

			$query = $this->db->query($sql);

			$result = $query->result_object();
		}

		$this->responsesuccess($this->lang->line('success'), $result);
	}

	// GET LOGO 
	public function company()
	{

		$url = base_url() . 'public/website/';

		$sql = "SELECT id, name, description, keywords, (CASE WHEN logo!='' THEN CONCAT('" . $url . "', logo) ELSE '' END) as logo, 
		
		(CASE WHEN logo_white!='' THEN CONCAT('" . $url . "', logo_white) ELSE '' END) as logo_white,
		
		(CASE WHEN shortcut!='' THEN CONCAT('" . $url . "', shortcut) ELSE '' END) as shortcut 
		
		FROM wstm_title WHERE id=1 AND status=1";

		$query = $this->db->query($sql);

		$data = $query->row_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}

	// GET setting LANGUAGE
	public function setting()
	{
		$status = isset($this->params['status']) ? $this->params['status'] : 1;

		$sql = "SELECT id, text_key, title, value, type, status 
		FROM wstm_setting WHERE status = " . $status;

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$list = ($list != null) ? $list :  array();

		$message = $this->lang->line('success');

		$this->responsesuccess($message, $list);
	}


	//LOGIN ADMIN
	public function adminLogin()
	{

		$data = $this->getdata();

		$is = false;

		$password = sha1($data['password']);

		$sql = "SELECT count(id) as count FROM hrtb_user WHERE email='" . $data['email'] . "' AND status = 1 AND password ='" . $password . "'";

		if ($this->db->query($sql)->row_object()->count == 1) {

			$sql = "SELECT * FROM hrtb_user WHERE  status = 1 AND email='" . $data['email'] . "'  AND password ='" . $password . "'";

			$query = $this->db->query($sql);

			$list = $query->row_object();

			$this->session->set_userdata('user', true);
			$this->session->set_userdata('user_id', $list->id);
			$this->session->set_userdata('user_name', $list->name);

			$is = true;
		}

		if ($is == true) {

			$this->responsesuccess($this->lang->line('loginSuccess'), $list);
			
		} else {

			$this->responsefailure($this->lang->line('failLogin'));
		}
	}

	/* GET SLIDES type - 1:  2:banner */
	public function getslide()
	{
		 $url = base_url() . 'public/slides/';

        $type = isset($this->params['type']) ? $this->params['type'] : 1;

        $sql = "SELECT id, name, link, title, description, orders, type, status , position, 
		
		(CASE WHEN images!='' THEN CONCAT('" . $url . "', images) ELSE '' END) AS images
		
		FROM wstm_slide WHERE status = 1 AND type = " . $type . " ORDER BY orders ASC ";

        $query = $this->db->query($sql);

        $list = $query->result_object();

        $message = $this->lang->line('success');

        $this->responsesuccess($message, $list);
	}
	
	// getProductGroup
	public function getProductGroupHome()
	{

		$sql = "SELECT id, name, link	FROM wstm_page	WHERE parent_id = 3 AND status = 1 ORDER by orders ASC LIMIT 5";

		$query = $this->db->query($sql);

		$data = $query->result_object();
		
		$url = base_url() . 'public/products/';
		
		if(!empty($data)){
		    
		    foreach ($data as $value) {

        		$sql = "SELECT t1.name, t1.link, t1.price, t1.price_sale, t1.percent, t2.link as parent_link, t1.views,
			
		  		(CASE WHEN t1.images != '' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images

				FROM pdtb_product AS t1

				LEFT JOIN wstm_page AS t2 ON t2.id = t1.page_id

				WHERE t1.page_id = " . $value->id . " OR t1.page_id IN (SELECT id FROM wstm_page WHERE parent_id = " . $value->id . ")

				ORDER BY t1.maker_date DESC

				LIMIT 10";

				// WHERE t1.page_id = " . $value->id . " 

				// WHERE t1.page_id IN (SELECT id FROM wstm_page WHERE parent_id = " . $value->id . ")

		        $query = $this->db->query($sql);

    			$value->list = $query->result_object();
    		}
		}
		

		$this->responsesuccess($this->lang->line('success'), $data);
	}


	// getProductGroup
	public function getProductGroup()
	{
		$sql = "SELECT id, title, description

				FROM wstm_page

				WHERE status = 1 AND id = 3";

		$query = $this->db->query($sql);

		$data = $query->row_object();

		if (($data != null) && ($data->id > 0)) {

			$sql = "SELECT id, name, link

					FROM wstm_page

					WHERE parent_id = " . $data->id;

			$query = $this->db->query($sql);

			$data->list = $query->result_object();
		}

		$this->responsesuccess($this->lang->line('success'), $data);
	}


	// getProductList
	public function getProductList()
	{
		$id = (isset($this->params['id'])) ? $this->params['id'] : 0;

		$limit = (isset($this->params['limit'])) ? $this->params['limit'] : 0;

		$url = base_url() . 'public/products/';

		$sql = "SELECT t1.name, t1.link, t1.price, t1.price_sale, t1.percent, t2.link as parent_link, t1.views,
			
		  		(CASE WHEN t1.images != '' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images

				FROM pdtb_product AS t1

				LEFT JOIN wstm_page AS t2 ON t2.id = t1.page_id

				WHERE t1.page_id IN (SELECT id FROM wstm_page WHERE parent_id = " . $id . ")
				
				LIMIT " . $limit;

		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}


	// getAboutUs
	public function getAboutUs()
	{
		$url = base_url() . 'public/pages/';

		$sql = "SELECT name,  link, description,title,

		(CASE WHEN images!='' THEN CONCAT('" . $url . "', images) ELSE '' END) AS images 
	
		FROM wstm_page 
		
		WHERE status = 1 AND id = 2 ";

		$query = $this->db->query($sql);

		$data = $query->row_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}

	// getPartner
	public function getPartner()
	{
		$url = base_url() . 'public/partner/';

		$sql = "SELECT company, link, (CASE WHEN logo!='' THEN CONCAT('" . $url . "', logo) ELSE '' END) AS logo 
	
		FROM tb_partner 
		
		WHERE status = 1 AND pin = 1 ORDER BY maker_date DESC ";

		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}

	//GET PRODUCT CATEGORY
	public function productCategory()
	{
		$url = base_url() . 'public/pages/';

		$sql = "SELECT id, name, link, 
		
		(CASE WHEN icon!='' THEN CONCAT('" . $url . "', icon) ELSE '' END) AS icon
		
		FROM wstm_page WHERE status =1 AND parent_id = 3 ORDER by orders ASC";

		$query = $this->db->query($sql);

		$data = $query->result_object();

		foreach ($data as $value) {

			$sql = "SELECT id, name, link
			
			FROM wstm_page WHERE status = 1 AND parent_id = " . $value->id;

			$query = $this->db->query($sql);

			$childCats = $query->result_object();

			$value->list = $childCats;
		}

		$this->responsesuccess($this->lang->line('success'), $data);
	}
	

    public function productGroup()
	{
		$url = base_url() . 'public/pages/';

		$sql = "SELECT id, name, link, parent_id, 
		
		(CASE WHEN icon!='' THEN CONCAT('" . $url . "', icon) ELSE '' END) AS icon
		
		FROM wstm_page WHERE status =1 AND type = 3 AND id != 3 ORDER by orders ASC";

		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}

	//getProduct
	public function getProduct()
	{
		$url = base_url() . 'public/products/';

		$urlpage = base_url() . 'public/pages/';

		$link = isset($this->params['link']) ? $this->params['link'] : "san-pham";

		$sql = "SELECT t1.id, t1.name, t1.link, t1.title, t1.description, t2.name as parent_name, t2.id as parent_id, t2.link as parent_link,
  			
		(CASE WHEN t1.images!='' THEN CONCAT('" . $urlpage . "', t1.images) ELSE '' END) AS images
			
 		FROM wstm_page AS t1
		
		LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id
		
		WHERE t1.status = 1 AND t1.type = 3 ";

		$sql .= " AND t1.link = '" . $link . "'";

		$query = $this->db->query($sql);

		$data = $query->row_object();

		if ($data != null) {

			$sql = " SELECT id FROM wstm_page WHERE parent_id = " . $data->id . " AND status=1 ";

			$query = $this->db->query($sql);

			$dataParent = $query->result_object();

			$sql = "SELECT t1.id, t1.name, t1.code, t1.page_id, t1.price_sale, t1.price, t1.views, t1.percent,
	
    		(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images ,
    		
			t1.link, t2.link as parent_link
    		
    		FROM pdtb_product AS t1 
    		
    		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
    		
    		WHERE t1.status = 1 ";

			if($link != 'san-pham'){
			    if (count($dataParent) > 0) {

				$sql .= " AND (";

				for ($i = 0; $i < count($dataParent); $i++) {

					$sql .= " t1.page_id = " . $dataParent[$i]->id . "";

					if ($i < count($dataParent) - 1) {

						$sql .= " OR ";
					}
				}

				$sql .= " ) ";
				
			} else {

				$sql .= " AND t1.page_id = " . $data->id;
			}
			}
			
			$sql .= " ORDER BY t1.maker_date DESC ";

			$query = $this->db->query($sql);

			$data->list = $query->result_object();
		}

		$this->responsesuccess($this->lang->line('success'), $data);
	}


	//Product detail
	public function getproductdetail()
	{
		$link = $this->params['link'];

		$url = base_url() . 'public/products/';

		$sql = "UPDATE pdtb_product SET  views = views + 1  where link='" . $link . "'";

		$this->db->query($sql);

		$sql = "SELECT t1.* , t2.id AS page_id, t3.link as parent_link, t3.name as parent_name,
		
		t2.name AS parent_name, t2.link AS pages_link
		
		FROM pdtb_product AS t1
		
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
		
		LEFT JOIN wstm_page AS t3 ON t3.id = t2.parent_id
		
		WHERE t1.link='" . $link . "' AND t1.status=1";

		$query = $this->db->query($sql);

		$data = $query->row_object();

		$sql = "SELECT t1.name, t1.price, t1.price_sale , t1.percent, t1.link, t1.views,
		
		(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images, 
		
		t2.link AS parent_link
		
		FROM pdtb_product AS t1
	
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
		
		WHERE t1.page_id='" . $data->page_id . "' AND t1.status=1 AND t1.link <> '" . $link . "' LIMIT 8";

		$query = $this->db->query($sql);

		$data->related = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}

	// getContent
	public function getContent()
	{
		$url = base_url() . 'public/pages/';

		$limit = isset($this->params['limit']) ? $this->params['limit'] : 0;

		$sql = "SELECT id, link, name, title , description, 
		
		(CASE WHEN images!='' THEN CONCAT('" . $url . "',images) ELSE '' END) AS images 
		
		FROM wstm_page 
		
		WHERE id = 5 AND status=1";

		$query = $this->db->query($sql);

		$data = $query->row_object();

		if ($data != null && $data->id > 0) {
		    
		    $url = base_url() . 'public/contents/';

			$sql = "SELECT t1.id, t1.name, t1.views, t1.description, t1.link,  t1.maker_date , 
		
				(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images ,
				
				t2.name as page_name, t2.link AS parent_link 
				
				FROM wstm_content as t1 
				
				LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
									
				WHERE t1.status = 1 ORDER BY t1.maker_date DESC";

			if ($limit > 0) {

				$sql .= " LIMIT 8";
			}

			$query = $this->db->query($sql);

			$data->list = $query->result_object();
		}

		$this->responsesuccess($this->lang->line('success'), $data);
	}

	//Content detail
	public function getContentDetail()
	{
        $link = isset($this->params['link']) ? $this->params['link'] : '';

        $url = base_url() . 'public/contents/';

        $url_file = base_url() . 'public/file/';

        $sql = "UPDATE wstm_content SET  views = views + 1  WHERE link = '" . $link . "'";

        $this->db->query($sql);

        $sql = "SELECT t1.id, t1.name, t1.description, t1.views, t1.link,
		
		t2.name as parent_name, t1.page_id, t1.detail, t1.maker_date, t1.file AS name_file, t1.source, t1.keywords,

		(CASE WHEN t1.file!='' THEN CONCAT('" . $url_file . "', t1.file) ELSE '' END) AS link_file,  
		
		(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images 
				
		FROM wstm_content as t1 
		
		LEFT JOIN wstm_page  AS t2 ON t1.page_id = t2.id 		
		
		WHERE t1.link='" . $link . "' AND t1.status=1";

        $query = $this->db->query($sql);

        $data = $query->row_object();

        if (!empty($data) && $data->id > 0) {

            $url = base_url() . 'public/contents/';

            $sql = "SELECT t1.id, t1.name, t1.views, t1.description, t1.link, t1.maker_date , t1.page_id, 
			
			(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images ,	
			
			t2.name as parent_name, t2.link AS parent_link 	
			
			FROM wstm_content as t1 		
			
			LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id		
			
			WHERE  t1.status = 1 AND t1.page_id = " . $data->page_id . " AND t1.id != " . $data->id . " 
			
			ORDER BY t1.maker_date DESC LIMIT 6";

            $query = $this->db->query($sql);

            $data->related = $query->result_object();
        }

        $this->responsesuccess($this->lang->line('success'), $data);
    }


	// getContentCategory
	public function getContentCategory()
	{

		$url = base_url() . 'public/contents/';

		$sql = "SELECT t1.id, t1.name, t1.link, t2.count FROM wstm_page AS t1
		
		LEFT JOIN ( SELECT count(id) as count , page_id   FROM wstm_content GROUP BY page_id)  AS t2 ON t1.id = t2.page_id
		
		WHERE t1.status =1 AND t1.type = 4 AND t1.id NOT IN ( SELECT parent_id FROM wstm_page)";

		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}

	// getDetailPagesByLink
	public function getDetailPagesByLink()
	{
		$url = base_url() . 'public/pages/';

		$link = isset($this->params['link']) ? $this->params['link'] : '';

		$sql = "SELECT id, link, description, name , detail , 
		
		(CASE WHEN images!='' THEN CONCAT('" . $url . "',images) ELSE '' END) AS images 
		
		FROM wstm_page WHERE link='" . $link . "' AND status=1";

		$query = $this->db->query($sql);

		$data = $query->row_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}


	/**
	 * 
	 * Create 
	 * Name: PHAN ANH VAN
	 * Date: 26/01/2021
	 * Note: add contact
	 * ------------------
	 * Edit: 
	 * Name:  
	 * Date: 
	 * Note:  
	 *
	 **/
     public function addContact() {
        $data = $this->getdata();
        $is = false;
        
        if (!empty($data)) {
            $data['maker_date'] = date('Y-m-d H:i:s');
            $is = $this->db->insert('wstb_contact', $data);
            
            if ($is == true) {
                $message = '';
                $message .= '----------------------------------------------';
                $message .= "<h3>Thông tin </h3>";
                $message .= "<p>Họ và tên: " . $data['name'] . "</p>";
                $message .= "<p>Số điện thoại: " . $data['phone'] . "</p>";
                $message .= "<p>Email: " . $data['email'] . "</p>";
                $message .= "<p>Nội dung: " . $data['message'] . "</p>";
                $subject = $data['subject'];
                $mail = $data['email'];
                $html = 'Cảm ơn bạn đã quan tâm đến chúng tôi. Chúng tôi sẽ liên hệ đến bạn sớm nhất.';
                $html .= "" . $message;
               // $this->sendmail($mail, $subject, $html); //send email khách hàng
                
                $detail = '';
                $detail .= "<p>Hệ thống vừa nhận được tin nhắn của khách hàng.</p>";
                $detail .= "" . $message;
               // $this->sendmail('', $subject, $detail);
            }
        }

        if ($is == true) {

            $this->responsesuccess($this->lang->line('success'));

        } else {

            $this->responsefailure($this->lang->line('failure'));
        }
    }

	//search
	public function search()
	{

		$option = isset($this->params['keywords']) ? $this->params['keywords'] : '';

		$option = urldecode($option);

		$url = base_url() . 'public/products/';

		$sqlNormal = "SELECT t1.name, t1.code,  t1.price_sale, t1.percent, t1.views,

		(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images ,
		
		t1.price,  t1.link, t2.link as parent_link
		
		FROM pdtb_product AS t1 
			
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
		
		WHERE t1.status = 1 AND ( t1.name like '%" . $option . "%' OR t1.description like '%" . $option . "%' OR t1.keywords like '%" . $option . "%') 
        
        ORDER BY (CASE WHEN t1.name LIKE '%" . $option . "%' THEN 1 
    	
        WHEN t1.description LIKE '%" . $option . "%'  THEN 2 ELSE 3 END) ";

		$sqlDetail = "SELECT t1.name, t1.code,  t1.price_sale, t1.percent, t1.views,

		(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images ,
		
		t1.price,  t1.link, t2.link as parent_link
		
		FROM pdtb_product AS t1 
			
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id	
		
		WHERE t1.status = 1 AND ( " . $this->processKeySearch('t1.name', $option) . " OR " . $this->processKeySearch('t1.description', $option) . " OR " . $this->processKeySearch('t1.keywords', $option) . " ) 
        
        ORDER BY (CASE WHEN t1.name LIKE '%" . $option . "%' THEN 1 
    	
        WHEN t1.description LIKE '%" . $option . "%'  THEN 2 ELSE 3 END) ";

		$query = $this->db->query($sqlNormal);

		$list = $query->result_object();

		if (count($list) <= 0) {

			$query = $this->db->query($sqlDetail);

			$list = $query->result_object();
		}

		$this->responsesuccess($this->lang->line('success'), $list);
	}

	function processKeySearch($field, $keywords)
	{

		$result = $field . ' like ' . "'%" . $keywords . "%'";

		$listKeywords = explode(' ', $keywords);

		for ($i = 0; $i < count($listKeywords); $i++) {

			$result .= " OR " . $field . ' like ';

			$result .= "'%" . $listKeywords[$i] . "%'";
		}

		return $result;
	}

	function createLink($str)
	{
		$unicode = array(
			'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
			'd' => 'đ',
			'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
			'i' => 'í|ì|ỉ|ĩ|ị',
			'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
			'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
			'y' => 'ý|ỳ|ỷ|ỹ|ỵ',
			'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
			'D' => 'Đ',
			'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
			'I' => 'Í|Ì|Ỉ|Ĩ|Ị',
			'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
			'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
			'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
		);

		foreach ($unicode as $nonUnicode => $uni) {
			$str = preg_replace("/($uni)/i", $nonUnicode, $str);
		}

		$str = $str;
		$str = strtolower(preg_replace('/[^a-zA-Z0-9\ ]/', '', $str));
		$str = preg_replace('/\s\s+/', ' ', trim($str));
		$str = str_replace(" ", "-", $str);

		return $str;
	}


	public function processApi(){
	    
	    $path =  './public/contents/';
		
		$list = $this->getdata();
		
		$data = $list['data'];
		
		$token = $list['token'];
		
		$is = false;

		$message = 'Nạp tin thất bại';
		
	    if(!empty($data)){
	        
	        $sql ="SELECT COUNT(id) AS count FROM wstm_setting WHERE value='".$token."'";
	        
	        if ($this->db->query($sql)->row_object()->count == 1) {
	            
                $data['maker_id'] = 1;
                
                $data['status'] = 1;
                
                $data['maker_date'] = date('Y-m-d H:i:s');
                
                $data['page_id'] = $data['group_id'] ;
                
                unset($data['group_id']);
                
                unset($data['images_seo']);
              
                $data['images'] = $this->processImagesApi($data['images'], $path);
               
        		$data['link']= (array_key_exists('link' , $data) && strlen($data['link']) > 0) ? removesign($data['link']) : removesign($data['name']);
        	 
        		$sql ="SELECT COUNT(id) AS count FROM wstm_content WHERE link='".$data['link']."'";
         
        		if ($this->db->query($sql)->row_object()->count == 0) {
        
        		    $is = $this->db->insert('wstm_content', $data);
    		    
    		        $message = ($is == true) ? 'Nạp vào thành công' : 'Thất bại';
        		    
        		}else{
        		    
        		    $message =  'Tin đã được nạp trước đó';
        		   
        		}
                
    	    } else {
	        
    	        $message = "Token bị sai";
    	    }
	    }else{
	        
	        $message = 'Không tồn tại dữ liệu';
	    }
		
		if ($is == true) {

			$this->responsesuccess($message,$data);
			
		} else {
			$this->responsefailure($message);
		}
	}
	
	function processImagesApi($image, $path) {
	    
	    $extension = substr(explode( '/', $image )[1],0, 3);
	    
	    $newname = (rand() * time() ). '.' . $extension;
	    
	    if(is_array(getimagesize($image))){
	        
	        @$file = file_get_contents($image);

			@$result = file_put_contents($path . $newname, $file);

	    }
	    
	   return $newname;
	 
    }

    	// getPageByLink
	public function getPageByLink()
	{	
		$url = base_url() . 'public/pages/';
		
		$link = isset($this->params['link']) ? $this->params['link'] : '';
		
		$sql = "SELECT id, link, description, name , detail , type,
		
		(CASE WHEN images!='' THEN CONCAT('" . $url . "',images) ELSE '' END) AS images 
		
		FROM wstm_page WHERE link='".$link."' AND status=1";

		$query = $this->db->query($sql);
		
		$data = $query->row_object();
	
		$this->responsesuccess($this->lang->line('success'), $data);
	
	}

		// SIGNUP
	public function signup(){
		$data = $this->getdata();
        $is = false;
		$res = array('status'=>0,'message'=> $this->lang->line('failure'),'data'=>array());
		$sql="SELECT count(id) as count FROM cstb_customer WHERE email='".$data['email']."'";
		$skip = false;
		if(array_key_exists('password',$data)){
			$data['password'] = SHA1($data['password']);
		}
		// if(count($data)>=1){
			if($this->db->query($sql)->row_object()->count == 0){
			    // $sql="SELECT count(id) as count FROM cstb_customer WHERE name='".$data['name']."'";
			    // if($this->db->query($sql)->row_object()->count == 0){
    				$data['maker_date'] = date('Y-m-d H:i:s');
    				$data['code'] = time();
    				$data['status'] = 1;
    				// $data['group_customer_id']=2;
    				$is = $this->db->insert('cstb_customer', $data);


    				// $sql1 ="SELECT id,name, email, phone, address  FROM cstb_customer WHERE email='".$data['email']."'";
    				// $query = $this->db->query($sql1);
    				// $list = $query->row_object();
    				// $res['data'] = $list;	
    				// $res['message'] =$this->lang->line('signup');
    				// $res['status'] = ($is==true) ? 1 : 0;
			    // }else{
			        $skip = false;
			    // }
			} else{
		        $skip = true;
		    }
		// }	

		if(!$skip){

			$sql = "SELECT name, id, email, phone, address 
					FROM cstb_customer 
					WHERE email='".$data['email']."'";

			$query = $this->db->query($sql);

			$data = $query->row_object();

			$this->responsesuccess($this->lang->line('success') , $data);
		}
		else{
		    // if($skip){
		    //     $this->responsefailure($this->lang->line('checkExitname'));
		    // }else{
		        $this->responsefailure($this->lang->line('checkExitEmail'));
		    // }
			// 
		}
	}

	//addcart
	public function addcart(){
	    
		$data=$this->getdata();
	
		$is=false;
		
		$isDCart=false;
		
		$id_cart = (isset($id_cart)) ? $id_cart : 0;
		
		$data['maker_date']= date('Y-m-d H:i:s');
		
		if($data != null){
		    
		    if(array_key_exists('cart',$data)){
			
    			$cart=$data['cart'];
    			
    			if(array_key_exists('id',$cart)){
    				
    				$cart = array_diff_key($cart,array('id' => $cart['id']));
    				
    			}
    			if($id_cart == 0){
    				$cart['code']= time();
    				
    				$is=$this->db->insert('ortb_cart', $cart);
    				
    				$sql="select id from ortb_cart where code='".$cart['code']."'";
    				$id_cart=$this->db->query($sql)->row_object()->id;
    			}
    			if($is == 0){
    				
    				$this->responsefailure($this->lang->line('failure'));die;
    			}
    		}
    		$res = array('status'=>0,'message'=>$this->lang->line('failure') , 'data'=>array('id' => $id_cart));
    		
    		if($id_cart > 0 && array_key_exists('cartdetail',$data)){
    			
    			$detail=$data['cartdetail'];
    	
    			
    			for($i = 0 ;$i < count($detail);$i++){
					$detail[$i]['cart_id'] = $id_cart;
					$detail[$i]['maker_date'] = date('Y-m-d H:i:s');
					unset($detail[$i]['images']);
    			}
    			$is = $this->db->insert_batch('ortb_cart_detail', $detail);
    			
    			
    		}
    
    		if($is==true){
    		    
    		    $finish = true;
    			if(array_key_exists('item',$data)){
    				$item=$data['item'];
    			}
				$dataCustomer = [];
				$dataCustome['address']= $cart['delivery_address'];
				$dataCustome['phone']= $cart['phone'];
		       
			   
				$this->db->where('id',$item['id']);
			    $checkUpdate = $this->db->update("cstb_customer", $dataCustome);
			
    			$email ='tn24635789@gmail.com';	
				
    			$message='';
    			$message.="<h3>Order receipt information : </h3></br>";
    			$message.="<p>Full name: ".$item['name']."</p>";
    			$message.="<p>Phone number: ".$cart['phone']."</p>";
    			$message.="<p>Email: ".$item['email']."</p>";
    			$message.="<p>Delivery address: ".$cart['delivery_address']."</p>";
    			
    			strlen($cart['note']) > 0 ? $message.="<p>Note: ".$cart['note']."</p>" : '';
    			$message.="<h3>Code orders: <strong>".$cart['code']."</strong> </h3>
    					<table style='border-collapse: collapse;'>
    						  <tr>
    							<th style='padding:10px;border: 1px solid #dddddd;'>Name</th>
    							<th style='padding:10px;border: 1px solid #dddddd;'>Quantity</th>
    							<th style='padding:10px;border: 1px solid #dddddd;'>Price</th>
    						  </tr>";
    						foreach ($item['data'] as $key => $v){
    							$message.="<tr>
    								<td style='padding:10px;border: 1px solid #dddddd;'>".$v["name"]."</td>
    								<td style='padding:10px;border: 1px solid #dddddd;'>".$v["amount"]."</td>
    								<td style='padding:10px;border: 1px solid #dddddd;'>".$v["price"]."</td>
    							  </tr>
    							";
    						}
    					$message.=" 
    						</table>
    					";
    				$detail='';
    				$detail.="<p>Have an order placed. Please go to the management system to check</p>";
    				$detail.="".$message;
    			
    				$detailcustomer='';
    				$detailcustomer.= "<h3>Thank you for ordering at NKN Store. We will contact you soon!</h3>".$message;
        			@$this->sendmail($email,"GioWebsite",$detail);
    				@$this->sendmail($email2,"NKN Store",$detail);
    				@$this->sendmail($item['email'],"THE ORDER NKN Store ",$detailcustomer);
    				$res['message']='Đặt hàng thành công. Vui lòng kiểm tra email của bạn.';
    				
    		}
    		
		}
	  
		$res['status'] = ($is == 0) ?  0 : 1;
		  	
		if($res['status'] == 1){
		    $this->responsesuccess($res['message'], array('code' => $cart['code']));
		}
		else{
			$this->responsefailure($this->lang->line('failure'));
		}
	}

	public function followCard(){
		
		$customer_id = $this->params['id'];
		
		$sql="SELECT t1.code, t1.day_start, t1.price_total , t1.delivery_status, t2.name 
		
		FROM ortb_cart AS t1 
		
		LEFT JOIN cstb_customer AS t2 ON t2.id = t1.customer_id 
		
		WHERE t1.customer_id=".$customer_id;
		
		$query = $this->db->query($sql);
		
		$data = $query->result_object();
		
		$this->responsesuccess($this->lang->line('success') , $data);
	}
	
	public function getCartDetail(){
		
		$code = $this->params['code'];
		
		$url = base_url().'public/products/';
		
		$sql="SELECT t1.id, t1.code, t1.day_start, t1.price_total , t1.delivery_status,
		
		t1.phone,  t1.delivery_address, t1.note, t1.amount_total, t2.name 
		
		FROM ortb_cart AS t1 
		
		LEFT JOIN cstb_customer AS t2 ON t2.id = t1.customer_id 
		
		WHERE t1.code=".$code;
		
		$query = $this->db->query($sql);
		
		$cart = $query->row_object();
		
		$sql="SELECT t1.* , t2.name, t2.code,(CASE WHEN t2.images!='' THEN CONCAT('".$url."',t2.images) ELSE '' END) as images, 
		
		t2.link, t3.link AS parent_link 
		
		FROM ortb_cart_detail AS t1 
		
		LEFT JOIN pdtb_product AS t2 ON t2.id = t1.product_id 
		
		LEFT JOIN wstm_page AS t3 ON t2.page_id= t3.id  WHERE t1.cart_id=".$cart->id;
		
		$query = $this->db->query($sql);
		
		$detail = $query->result_object();
		
		$data = array('cart'=> $cart,'detail'=>$detail);
		
		$this->responsesuccess($this->lang->line('success') , $data);
	}

	// login
	public function apilogin() {

		$data=$this->getdata(); 

		$is= false;

		$list = array();
		
		if(!empty($data)){

			$sql = "SELECT name, id, email, phone, address 
					FROM cstb_customer 
					WHERE email='".$data['email']."' AND password='".SHA1($data['password'])."' AND status = 1";

			$query = $this->db->query($sql);

			$list = $query->row_object();

			if(!empty($list)){

				$this->session->set_userdata('customer',true);

				$this->session->set_userdata('customer_id',$list->id);

				$this->session->set_userdata('customer_name',$list->name);

				$is = true;

			}
			
		}
	    
		if($is){
		
			$this->responsesuccess($this->lang->line('loginSuccess') ,$list);
		}

		else{

			$this->responsefailure($this->lang->line('failLogin'));

		}
	}

	// checkTokenResetPassword
	public function checkTokenResetPassword () {
		
		$is = false;
		
		$sql = "SELECT id FROM cstb_customer WHERE token = '".$this->params['value_token']."' ";
		
		$data = $this->db->query($sql)->row_object();
			
		$is = array_key_exists('id',$data) ? 1 : 0;
		
		if($is ==1){
			$this->responsesuccess($this->lang->line('success'), $data);
		}else{
			$this->responsefailure($this->lang->line('failure'));
		}
	}

	//resetpassword
	public function resetpassword(){
		$data = $this->getdata();
		
		$is =false;
	
		$sql="SELECT count(id) as count, id FROM cstb_customer where email='".$data['email']."' GROUP BY id ";
	
		$id = $this->db->query($sql)->row_object()->id;
		if(count($data)>0){
			
			if($this->db->query($sql)->row_object()->count == 1){
				
				$data['token']=sha1(time());
				$this->db->where('id',$id);
				$is = $this->db->update('cstb_customer', $data);
			}
			if($is == true){
				$message= '<!DOCTYPE html>
					<html>
					<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
					<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

					</head>
					<body>

					<div class="col-6 offset-3">
						<p>Chào quý khách!</p>
						<p>	Chúng tôi đã nhận được yêu cầu thay đổi mật khẩu của quý khách.</p>

						<p>	Xin hãy click vào đường dẫn sau để đổi mật khẩu: https://gio2.giowebsite.com/doi-mat-khau/'.$data['token'].'</p>
						<p>	Mọi thắc mắc và góp ý vui lòng liên hệ với chúng tôi qua email: support@chips.vn </p>

						<p>	Trân trọng, </p>
					</div>			

					</body>
					</html>';
				$email = $data['email'];
				@$this->sendmail($email,"GioWebsite",$message);
				$this->responsesuccess('Quá Trình của bạn thành công. Bạn hãy kiểm tra lại email.');
			}
		}
		$this->responsefailure($this->lang->line('failure'));
	}

	//   getRowCustomer
	public function getRowCustomer()
	{	
		$id = $this->params['id'];
		
		$sql="SELECT id, name, email, phone, address, sex FROM cstb_customer WHERE id=".$id;
		
		$query = $this->db->query($sql);
		
		$list = $query->row_object();
		
		$this->responsesuccess($this->lang->line('success') , $list);
	
	}
	
	//   updateCustomer
	public function updateCustomer()
	{	
		$data = $this->getdata();
		
        $is = false;
		
        if($data['id'] && $data['id'] > 0){
			
			$sql="SELECT count(id) as count FROM cstb_customer WHERE id = ".$data['id'];
			
			// if(count($data)>1){
				
				if($this->db->query($sql)->row_object()->count == 1){
					
					$this->db->where('id',$data['id']);
					
					$is = $this->db->update("cstb_customer", $data);
					
					$is = ($is==true) ? 1 : 0;
					
					$sql="SELECT id, name, email, phone, address, sex FROM cstb_customer WHERE id=".$data['id'];
		
					$query = $this->db->query($sql);
					
					$list = $query->row_object();
					
					if(!empty($list)){
						$this->session->set_userdata('customer',true);
						$this->session->set_userdata('customer_id',$list->id);
						$this->session->set_userdata('customer_name',$list->name);
						$is = true;
					}
				}
			// 	else{
			// 		$this->responsefailure($this->lang->line('checkExitPhone'));
			// 	}
			// }
		}
		
		if($is ==1 ){
			$this->responsesuccess($this->lang->line('success'), $list);
		}
		else{
			$this->responsefailure($this->lang->line('failure'));
		}
	
	}

	// changePasswordCustomer
	public function changePasswordCustomer()
	{
		$is = false;
		
		$data = $this->getdata();
		
		if($data['id'] && $data['id'] > 0){
			
			if(array_key_exists('password',$data)){
				$data['password'] = SHA1($data['password']);
			}
			
			$sql = "SELECT count(id) as count FROM cstb_customer WHERE password = '".$data['password']."' ";
			
			if ($this->db->query($sql)->row_object()->count > 0) {
				
				$this->db->where('id', $data['id']);
		
				$is = $this->db->update('cstb_customer', array('password'=> sha1($data['password_new'])));
				
				$is = ($is==true) ? 1 : 0;
			}
		}
		
		if($is ==1 ){
			
			$this->responsesuccess($this->lang->line('success'));
		}
		else{
			$this->responsefailure($this->lang->line('failurePasswordOld'));
		}
	}
}

