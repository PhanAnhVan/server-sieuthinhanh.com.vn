<!doctype html>
<html lang="vi" translate="no">

<head>
    <meta charset="utf-8">
    <meta name="google" content="notranslate">
    <title></title>
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#fff">
    <meta name="author" content="Chips">
    <meta http-equiv="Cache-control" content="public">
    <?php echo $seo; ?>
    <script>window['globals'] = window['globals'] ? window['globals'] : {}</script>

    <meta name="google-site-verification" content="hTGrYDUnM3ma5BLbIS_8uTIF8_sTO9JwH39IclbtA_I" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-QC6KQMGBR5"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-QC6KQMGBR5');
    </script>

    <!-- ==================== Bootstrap ==================== -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" />

    <!-- ==================== Font Awesome ==================== -->
    <link rel="stylesheet" href="./assets/fontawesome/all.min.css" />
    
    <link rel="stylesheet" href="styles.4c17febd71593cc863e2.css">
</head>

<body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    
    <app-root></app-root>

    <!-- ==================== Bootstrap ==================== -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"
        integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF"
        crossorigin="anonymous"></script>
        
    <script src="runtime.42327f2345dcd57bf066.js" defer=""></script>
    <script src="polyfills-es5.f7a79d038046d557e576.js" nomodule="" defer=""></script>
    <script src="polyfills.fa35c98f29d212b6b037.js" defer=""></script>
    <script src="main.0609f48653f181e99e04.js" defer=""></script>
    
    <!-- ==================== Fancybox ==================== -->
    <link rel="stylesheet" href="https://unpkg.com/@fancyapps/ui/dist/fancybox.css" />
    <script src="https://unpkg.com/@fancyapps/ui/dist/fancybox.umd.js"></script>

    <!-- ==================== App ==================== -->
    <script type="text/javascript" src="./assets/js/js.js"></script>

    <!-- ==================== Date Picker ==================== -->
    <link rel="stylesheet" href="https://unpkg.com/ngx-bootstrap/datepicker/bs-datepicker.css">

    <!-- ==================== CKEditor ==================== -->
    <script src="//cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;1,400&display=swap"
        rel="stylesheet">
</body>

</html>